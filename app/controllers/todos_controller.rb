class TodosController < ApplicationController
  def index
    @projects = Project.all
    @todo = Todo.new
  end

  def update
    @todo = Todo.find(params[:id])
  end

  def new
    @projects = Project.all
  end

  def show
    redirect_to todos_path
  end

  def create
    @todo = Todo.new(params.require(:todo).permit(:text, :project_id))

    @todo.save
    redirect_to @todo
  end
end
